<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  xmlns:dxa="http://www.deltaxml.com/ns/non-namespaced-attribute"
  xmlns:dxx="http://www.deltaxml.com/ns/xml-namespaced-attribute"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:deltajava="java:com.deltaxml.samples.CompareBinary"
  version="2.0">
  
  <!-- This stylesheet demonstrates how xhtml images (referred to using img/@src) can be byte-wise
      compared as a comparison post-process.  This code will need adaption for other languages or grammars, for
      example other formats may use @href to refer to images or other binary content.  -->

  <!-- Note that the code tends to output @src attributes using resolved or relativized URIs.  This means
       that when the input may have said <img src="diagram.png"/>, the output will tend to say:
       <img src="file:///home/user/subfolder/diagram.png"/> or similar.  In certain cases this is necessary in
       order to see differences which are not caused by the @src attribute changing, but instead by comparing files
       in different locations so that the URI resolves to different files which may have different binary content.
       Another consequence of using resolved URIs in the result is that the result can be written in a different filesystem
       location to the inputs and images should still resolved. -->

  <xsl:import href="dx2-extract-version-moded.xsl"/>
  
  <xsl:template match="@* | node()" mode="#default">
    <xsl:copy>
      <xsl:apply-templates select="@*, node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- As well as allowing @xml:base flow through the pipeline also allow
    base URIs to be specified directly as params to the filter.  -->
  <xsl:param name="baseAOverride" as="xs:string" select="''"/>
  <xsl:param name="baseBOverride" as="xs:string" select="''"/>
  
  <xsl:variable name="baseUriA" select="if ($baseAOverride ne '') 
                                         then $baseAOverride else if(exists(/*/@xml:base)) 
                                         then /*/@xml:base 
                                         else /*/deltaxml:attributes/dxx:base/deltaxml:attributeValue[@deltaxml:deltaV2='A']"/>  

  <xsl:variable name="baseUriB" select="if ($baseBOverride ne '') 
                                         then $baseBOverride else if (exists(/*/@xml:base)) 
                                         then /*/@xml:base 
                                         else /*/deltaxml:attributes/dxx:base/deltaxml:attributeValue[@deltaxml:deltaV2='B']"/>
  
  <xsl:variable name="REFS_EQUAL" as="xs:integer" select="1"/>
  <xsl:variable name="REFS_DIFFERENT" as="xs:integer" select="0"/>
  <xsl:variable name="REFS_UNCOMPARABLE" as="xs:integer" select="2"/>
  
  <xsl:template match="xhtml:img[@src]">
    <xsl:variable name="Aresolved" select="resolve-uri(@src, $baseUriA)" as="xs:anyURI"/>
    <xsl:variable name="Bresolved" select="resolve-uri(@src, $baseUriB)" as="xs:anyURI"/>
    <xsl:choose>
      <xsl:when test="$Aresolved eq $Bresolved">
        <!-- when they resolve to the same thing, no point in doing binary comparison -->
        <xsl:copy>
          <xsl:attribute name="src" select="$Bresolved"/>
          <xsl:apply-templates select="@* except @src, node()"/>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="binaryCompareResult" as="xs:integer" select="deltajava:equalRefs(xs:string($Aresolved), xs:string($Bresolved))"/>
        <xsl:choose>
          <xsl:when test="$binaryCompareResult eq $REFS_EQUAL">
            <xsl:copy>
              <xsl:attribute name="src" select="$Bresolved"/>
              <xsl:apply-templates select="@* except @src, node()"/>
            </xsl:copy>
          </xsl:when>
          <xsl:otherwise>
            <!-- the xsl:copy statements below are similar to, and derived from, those in the extract-A-with-delta
            named templates in dx2-extract-version-moded.xsl.  However, we also want to add specific src attributes, based
            not on the @src in the input, but on the @src after resolving/relativizing using the associated base URIs. -->
            <xsl:copy copy-namespaces="no">
              <xsl:attribute name="deltaxml:deltaV2" select="'A'"/>
              <xsl:attribute name="src" select="$Aresolved"/>
              <xsl:apply-templates select="@* except (@deltaxml:deltaV2, @src)"/>
              <xsl:apply-templates select="deltaxml:attributes/*" mode="A"/>
              <xsl:apply-templates select="node() except deltaxml:attributes" mode="A"/>
            </xsl:copy>
            <xsl:copy copy-namespaces="no">
              <xsl:attribute name="deltaxml:deltaV2" select="'B'"/>
              <xsl:attribute name="src" select="$Bresolved"/>
              <xsl:apply-templates select="@* except (@deltaxml:deltaV2, @src)"/>
              <xsl:apply-templates select="deltaxml:attributes/*" mode="B"/>
              <xsl:apply-templates select="node() except deltaxml:attributes" mode="B"/>
            </xsl:copy>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="xhtml:img[deltaxml:attributes/dxa:src[count(deltaxml:attributeValue) eq 2]]">
    <xsl:variable name="Aref" as="xs:string" 
      select="deltaxml:attributes/dxa:src/deltaxml:attributeValue[@deltaxml:deltaV2='A']"/>
    <xsl:variable name="Bref" as="xs:string"
      select="deltaxml:attributes/dxa:src/deltaxml:attributeValue[@deltaxml:deltaV2='B']"/>
    <xsl:variable name="Aresolved" select="resolve-uri($Aref, $baseUriA)" as="xs:anyURI"/>
    <xsl:variable name="Bresolved" select="resolve-uri($Bref, $baseUriB)" as="xs:anyURI"/>
    <xsl:choose>
      <xsl:when test="$Aresolved eq $Bresolved">
        <!-- when they resolve to the same thing, no point in doing binary comparison. -->
        <xsl:copy>
          <xsl:attribute name="src" select="$Bresolved"/>
          <xsl:apply-templates select="@*"/>
          <xsl:apply-templates select="deltaxml:attributes/* except deltaxml:attributes/dxa:src"/>
          <xsl:apply-templates select="node() except deltaxml:attributes"/>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="binaryResult" as="xs:integer" select="deltajava:equalRefs(xs:string($Aresolved), xs:string($Bresolved))"/>
        <xsl:choose>
          <xsl:when test="$binaryResult eq $REFS_EQUAL">
            <xsl:copy>
              <!-- We've different images which are the same, we'll make an arbitrary decision and
                   point-to/present the (resolved) B version in the result -->
              <xsl:attribute name="src" select="$Bresolved"/>
              <xsl:apply-templates select="@*"/>
              <xsl:apply-templates select="deltaxml:attributes/* except deltaxml:attributes/dxa:src"/>
              <xsl:apply-templates select="node() except deltaxml:attributes"/>
            </xsl:copy>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy copy-namespaces="no">
              <xsl:attribute name="deltaxml:deltaV2" select="'A'"/>
              <xsl:attribute name="src" select="$Aresolved"/>
              <xsl:apply-templates select="@* except @deltaxml:deltaV2"/>
              <xsl:apply-templates select="deltaxml:attributes/* except deltaxml:attributes/dxa:src" mode="A"/>
              <xsl:apply-templates select="node() except deltaxml:attributes" mode="A"/>
            </xsl:copy>
            <xsl:copy copy-namespaces="no">
              <xsl:attribute name="deltaxml:deltaV2" select="'B'"/>
              <xsl:attribute name="src" select="$Bresolved"/>
              <xsl:apply-templates select="@* except @deltaxml:deltaV2"/>
              <xsl:apply-templates select="deltaxml:attributes/* except deltaxml:attributes/dxa:src" mode="B"/>
              <xsl:apply-templates select="node() except deltaxml:attributes" mode="B"/>
            </xsl:copy>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>