# Image and Binary Comparison
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

An example that illustrates how referenced images and other binary resources can be compared.  Further documentation on the process/mechanisms
used in this sample can be found on our documentation site: [Image and Binary Comparison](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/image-and-binary-comparison).


The sample code shows two methods of performing image comparison. The first method uses the Pipelined Comparator, and specifies input and output 
filters with a DXP file. The second method uses the API exposed by the Document Comparator.

If you have Ant installed, use the build script provided to run the sample. The script has been designed with the assumption that
this code has been checked-out, cloned or unzipped into the samples directory of the XML-Compare release, so that the `build.xml`
script is two levels below the top level release directory that contains the jar files.

The following command will generate output for the two methods in the results directory. Simply type the following command to run the comparison pipeline:

```
ant run
```

To run just the Pipelined Comparator sample and produce the output files in the PipelinedComparatorResult directory, type the following command.

```
ant run-dxp
```

This ant build script will generate a jar file that acts as a kind of extension or 'plugin' to the command-line app. Please examine the script to 
see how this mechanism works. It is particularly useful when DXP pipelines make use of Java code as either filters or XSLT extension functions and 
allows the simpler 'java -jar' invocation to be used.

To run just the Document Comparator API sample and produce the output files in the DocumentComparatorResult directory, type the following command.

```
ant run-dc
```

To clean up the sample directory, run the following command in Ant.

```
ant clean
```

If you don't have Ant installed, you can run the Document Comparator API sample from a command line by issuing the following commands from the 
sample directory (ensuring that you use the correct directory and class path separators for your operating system and replacing x.y.z with the 
major.minor.patch version number of your release e.g. deltaxml-10.0.0.jar).

```
javac -cp class:../../deltaxml-x.y.z.jar:../../saxon9pe.jar -d bin ./src/java/com/deltaxml/samples/ImageCompare.java
java -cp class:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../icu4j.jar:../../resolver.jar:./bin/com/deltaxml/samples/ com.deltaxml.samples.ImageCompare test1/f1.xhtml test1/f2.xhtml test1/DocumentComparatorResult/f1-f2-result.html
java -cp class:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../icu4j.jar:../../resolver.jar:./bin/com/deltaxml/samples/ com.deltaxml.samples.ImageCompare test2/doc1.xhtml test2/doc2.xhtml test2/DocumentComparatorResult/doc1-doc2-result.html
```


