// Copyright (c) 2024 Deltaman group limited. All rights reserved.
package com.deltaxml.samples;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * An XSLT extension class, designed to be used with SaxonPE/EE in DeltaXML Comparison Pipelines. Can be used to compare binary
 * files or more generally resources (jpeg/gif/png images, formulas...) referenced from xml content.
 *
 * Provides a binary byte-wise comparison, does not do clever stuff like identifying images which look the same or similar!
 */
public class CompareBinary {

  /**
   * Three possible results from the compare method.
   */
  private static final int HREFS_DIFFERENT= 0;
  private static final int HREFS_EQUAL= 1;
  private static final int HREFS_UNCOMPARABLE= 2;

  /**
   */
  public static int equalRefs(String ref1, String ref2) {
    URI uri1, uri2;
    try {
      String refa=ref1.replaceAll("\\s", "%20");
      String refb=ref2.replaceAll("\\s", "%20");
      uri1= new URI(refa);
      uri2= new URI(refb);
    } catch (URISyntaxException use) {
      return HREFS_UNCOMPARABLE;
    }

    if (uri1.getScheme().equals("file") && uri2.getScheme().equals("file")) {
      // If we have the common case of files on disk we can take some shortcuts,
      // eg: the OS can tell us whether they exist and their sizes
      // rather than having to start reading and processing streams

      File file1= new File(uri1);
      File file2= new File(uri2);

      if (!file1.exists() || !file2.exists()) {
        return HREFS_UNCOMPARABLE;
      }
      long file1Size= file1.length();
      long file2Size= file2.length();

      if (file1Size != file2Size) {
        // different sizes, MUST be different files
        return HREFS_DIFFERENT;
      }
    }
    InputStream is1, is2;
    try {
      is1= uri1.toURL().openStream();
      is2= uri2.toURL().openStream();
    } catch (MalformedURLException mue) {
      return HREFS_UNCOMPARABLE;
    } catch (IOException ioe) {
      return HREFS_UNCOMPARABLE;
    }
    // we've now got input streams, we can load them into byte-arrays
    // for comparison
    try {
      byte[] buf1;
      byte[] buf2;

      ByteArrayOutputStream baos= new ByteArrayOutputStream();
      BufferedInputStream bis= new BufferedInputStream(is1);
      int byteRead;
      while ((byteRead= bis.read()) != -1) {
        baos.write(byteRead);
      }
      buf1= baos.toByteArray();

      // reset vars to read is2
      byteRead= 0;
      baos= new ByteArrayOutputStream();
      bis= new BufferedInputStream(is2);

      while ((byteRead= bis.read()) != -1) {
        baos.write(byteRead);
      }
      buf2= baos.toByteArray();

      if (buf1.length != buf2.length) {
        // different sizes, MUST be different
        return HREFS_DIFFERENT;
      }

      for (int i= 0; i < buf1.length; i++) {
        if (buf1[i] != buf2[i]) {
          return HREFS_DIFFERENT;
        }
      }
    } catch (IOException ioe) {
      return HREFS_UNCOMPARABLE;
    }
    // We've exhausted everything, they must be equal!
    return HREFS_EQUAL;
  }
}
