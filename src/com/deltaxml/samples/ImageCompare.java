// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.samples;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.sf.saxon.s9api.Serializer;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.FilterClassInstantiationException;
import com.deltaxml.core.FilterConfigurationException;
import com.deltaxml.core.FilterParameterizationException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.core.PipelinedComparatorError;
import com.deltaxml.cores9api.ComparisonCancelledException;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.DocumentComparator;
import com.deltaxml.cores9api.DocumentComparator.ExtensionPoint;
import com.deltaxml.cores9api.DuplicateStepNameException;
import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelineLoadingException;
import com.deltaxml.cores9api.PipelineSerializationException;
import com.deltaxml.cores9api.config.ModifiedWhitespaceBehaviour;
import com.deltaxml.cores9api.config.LexicalPreservationConfig;
import com.deltaxml.cores9api.config.PresetPreservationMode;

public class ImageCompare {

  public static void main(String[] args) throws ParserInstantiationException, FilterConfigurationException,
      ComparatorInstantiationException, FilterClassInstantiationException, ComparisonException, FilterProcessingException,
      PipelineLoadingException, PipelineSerializationException, LicenseException, FileNotFoundException,
      PipelinedComparatorError, IOException, ComparisonCancelledException {

    // initialise files
    File f1= new File(args[0]);
    File f2= new File(args[1]);
    File result= new File(args[2]);

    DocumentComparator dc= new DocumentComparator();

    dc.setOutputProperty(Serializer.Property.INDENT, "yes");
    dc.setOutputProperty(Serializer.Property.METHOD, "html");
    // the VERSION property below is for HTML5 and is only supported by Saxon 9.5
    dc.setOutputProperty(Serializer.Property.VERSION, "5.0");

    dc.getResultReadabilityOptions().setElementSplittingEnabled(false);
    dc.getResultReadabilityOptions().setModifiedWhitespaceBehaviour(ModifiedWhitespaceBehaviour.NORMALIZE);
    
    LexicalPreservationConfig lexConfig= new LexicalPreservationConfig(PresetPreservationMode.ROUND_TRIP);
    lexConfig.setPreserveIgnorableWhitespace(false);
    lexConfig.setPreserveXMLDeclaration(false);
    lexConfig.setPreserveDocumentLocation(true);

    dc.setLexicalPreservationConfig(lexConfig);

    FilterStepHelper fsHelper= dc.newFilterStepHelper();
    FilterChain infilters= fsHelper.newFilterChain();
    FilterChain outfilters= fsHelper.newFilterChain();

    try {
      outfilters.addStep(fsHelper.newFilterStep(new File("xhtml-binary-image-compare.xsl"),
                                                "xhtml-binary-image-compare"));
      outfilters.addStep(fsHelper.newFilterStepFromResource("/xsl/xhtml/dx2-xhtml-outfilter.xsl", "x2-xhtml-outfilter"));
      outfilters.getStep("x2-xhtml-outfilter").setParameterValue("includeButtons", "true");
      outfilters.addStep(fsHelper.newFilterStepFromResource("/xsl/xhtml/dx2-xhtml-clean-house.xsl", "dx2-xhtml-clean-house"));

      dc.setExtensionPoint(ExtensionPoint.OUTPUT_FINAL, outfilters);
    } catch (DuplicateStepNameException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalStateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (FilterParameterizationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    dc.compare(f1, f2, result);
  }
}
